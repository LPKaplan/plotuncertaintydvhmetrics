﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using win = System.Windows;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;
using OxyPlot.Wpf;
using OxyPlot.Annotations;
using VMS.TPS.Common.Model.API;
using VMS.TPS.Common.Model.Types;

namespace PlotUncertaintyDVHMetrics
{
    public class MainViewModel
    {
        #region Properties
        public IEnumerable<Structure> Structures { get; private set; }
        public IEnumerable<PlanSetup> PlansInScope { get; private set; }
        public IEnumerable<PlanName> PlanCollection { get; set; }
        public PlotModel PlotModel { get; private set; }
        public string ChosenStructureId { get; set; }
        public string ChosenPlotType { get; set; }
        public double NumericInput { get; set; }
        #endregion

        // Constructor
        public MainViewModel(ScriptContext context)
        {
            // Set properties
            PlansInScope = GetPlansInScope(context);
            Structures = GetPlanStructures(context);
            PlanCollection = GetPlanCollection(PlansInScope);
            PlotModel = CreatePlotModel();

        }

        #region Private methods for setting properties
        // Get a list of all plans in the current context
        private IEnumerable<PlanSetup> GetPlansInScope(ScriptContext context)
        {
            var plans = context.PlansInScope != null
                ? context.PlansInScope : null;

            return plans.Where(p => p.IsDoseValid);
        }

        // Get a list of all structures in the currently loaded structure set
        private IEnumerable<Structure> GetPlanStructures(ScriptContext context)
        {
            var plan = context.PlanSetup != null ? context.PlanSetup : null;
            return plan.StructureSet != null
                ? plan.StructureSet.Structures : null;
        }

        // Make list of PlanName objects. The PlanName class implements INotifyPropertyChanged. 
        // This is so that the script can tell if a plan is checked or unchecked. 
        // Used to uncheck all plans when switching between plot views.
        private IEnumerable<PlanName> GetPlanCollection(IEnumerable<PlanSetup> plansInScope)
        {
            var List = new List<PlanName>();
            foreach (var plan in plansInScope)
            {
                var planName = new PlanName() { Name = plan.Id };
                List.Add(planName);
            }
            return List;
        }

        // Create an empty PlotModel
        private PlotModel CreatePlotModel()
        {
            PlotModel plotModel = new PlotModel();
            return plotModel;
        }
        #endregion

        #region Private methods for manipulating the PlotModel

        // Find a plan series
        private OxyPlot.Series.Series FindSeries(string id)
        {
            return PlotModel.Series.FirstOrDefault(x =>
            x.Tag.ToString().Contains(id));
        }

        // Find an annotation (line or text in the plot)
        private OxyPlot.Annotations.Annotation FindAnnotation(string id)
        {
            return PlotModel.Annotations.FirstOrDefault(x =>
            (string)x.Tag == id);
        }

        // Find index of plan in PlansInScope, used for placement of boxes and bars on the x-axis
        private double PlanIndex(PlanSetup plan)
        {
            List<PlanSetup> PlanList = PlansInScope.ToList();
            string id = plan.Id;

            return (double)PlanList.FindIndex(pln => pln.Id == id);
        }

        // Update plot when data is added or removed
        private void UpdatePlot()
        {
            PlotModel.InvalidatePlot(true);
        }
        #endregion

        #region Public general methods

        // Set up the correct axes
        public void ShowPlot()
        {
            IEnumerable<string> planIDs = PlansInScope.Select(p => p.Id);

            switch (ChosenPlotType)
            {
                case "Empty":
                    PlotModel.Title = "Choose a plot to display:";
                    break;

               
                case "Mean structure dose for all uncertainty scenarios":
                    CategoryScatterPlot meanBoxPlot = new CategoryScatterPlot();
                    meanBoxPlot.SetAxes(PlotModel, -0.5, PlansInScope.Count() - 0.5, 0, 0, "PlanID", new string[] { "Dose [Gy]" }, planIDs);
                    meanBoxPlot.SetLegend(PlotModel, false, LegendPlacement.Outside, LegendPosition.BottomCenter, LegendOrientation.Horizontal);
                    meanBoxPlot.SetTitle(PlotModel, "Mean dose in all uncertainty scnarios");
                    break;

                case "Vx for all uncertainty scenarios (absolute dose)":
                    CategoryScatterPlot VxAbsScatterPlot = new CategoryScatterPlot();
                    VxAbsScatterPlot.SetAxes(PlotModel, -0.5, PlansInScope.Count() - 0.5, 0, 0, "PlanID", new string[] { "Volume [%]" }, planIDs);
                    VxAbsScatterPlot.SetLegend(PlotModel, false, LegendPlacement.Outside, LegendPosition.BottomCenter, LegendOrientation.Horizontal);
                    VxAbsScatterPlot.SetTitle(PlotModel, "Vx in all uncertainty scnarios");
                    break;

                case "Vx for all uncertainty scenarios (relative dose)":
                    CategoryScatterPlot VxRelScatterPlot = new CategoryScatterPlot();
                    VxRelScatterPlot.SetAxes(PlotModel, -0.5, PlansInScope.Count() - 0.5, 0, 0, "PlanID", new string[] { "Volume [%]" }, planIDs);
                    VxRelScatterPlot.SetLegend(PlotModel, false, LegendPlacement.Outside, LegendPosition.BottomCenter, LegendOrientation.Horizontal);
                    VxRelScatterPlot.SetTitle(PlotModel, "Vx in all uncertainty scnarios");
                    break;

                case "Dx for all uncertainty scenarios (absolute volume)":
                    CategoryScatterPlot DxAbsScatterPlot = new CategoryScatterPlot();
                    DxAbsScatterPlot.SetAxes(PlotModel, -0.5, PlansInScope.Count() - 0.5, 0, 0, "PlanID", new string[] { "Dose [Gy]" }, planIDs);
                    DxAbsScatterPlot.SetLegend(PlotModel, false, LegendPlacement.Outside, LegendPosition.BottomCenter, LegendOrientation.Horizontal);
                    DxAbsScatterPlot.SetTitle(PlotModel, "Dx in all uncertainty scnarios");
                    break;

                case "Dx for all uncertainty scenarios (relative volume)":
                    CategoryScatterPlot DxRelScatterPlot = new CategoryScatterPlot();
                    DxRelScatterPlot.SetAxes(PlotModel, -0.5, PlansInScope.Count() - 0.5, 0, 0, "PlanID", new string[] { "Dose [Gy]" }, planIDs);
                    DxRelScatterPlot.SetLegend(PlotModel, false, LegendPlacement.Outside, LegendPosition.BottomCenter, LegendOrientation.Horizontal);
                    DxRelScatterPlot.SetTitle(PlotModel, "Dx in all uncertainty scnarios");
                    break;

            }
        }

        // Add series when a plan is checked
        public void AddPlanItem(PlanSetup plan)
        {
            // If no structure is chosen
            if (ChosenStructureId == null)
            {
                win.MessageBox.Show("Please choose a structure for metric evaluation!");
                return;
            }
            // If the checked plan does not have the chosen structure in its structure set:
            else if (!plan.StructureSet.Structures.Any(str => (str.Id == ChosenStructureId & str.IsEmpty == false)))
            {
                win.MessageBox.Show("The plan is not connected to a structure with the chosen ID, or the structure has no segment.");
                return;
            }
            // If no dose is calculated
            else if (plan.Dose == null)
            {
                win.MessageBox.Show("The plan has no calculated dose!");
                return;
            }
            // If everything is OK add the correct series type to the plot
            else
            {
                // Ensure dose is given in Gy
                plan.DoseValuePresentation = DoseValuePresentation.Absolute;

                Structure structure = plan.StructureSet.Structures.First(s => s.Id == ChosenStructureId);

                switch (ChosenPlotType)
                {
                    case "Mean structure dose for all uncertainty scenarios":
                        AddMeanDoseScatterSeries(plan, structure);
                        break;

                    case "Dx for all uncertainty scenarios (absolute volume)":
                        AddDxDoseScatterSeries(plan, structure, NumericInput, true);
                        break;

                    case "Dx for all uncertainty scenarios (relative volume)":
                        AddDxDoseScatterSeries(plan, structure, NumericInput, false);
                        break;

                    case "Vx for all uncertainty scenarios (absolute dose)":
                        AddVxScatterSeries(plan, structure, NumericInput, true);
                        break;

                    case "Vx for all uncertainty scenarios (relative dose)":
                        AddVxScatterSeries(plan, structure, NumericInput, false);
                        break;
                }

                UpdatePlot();
            }
        }

        // Remove series from plot when box is unchecked
        public void RemovePlanItem(PlanSetup plan)
        {
            while (PlotModel.Series.Any(ser => (string)ser.Tag == plan.Id))
            {
                var series = FindSeries(plan.Id);
                PlotModel.Series.Remove(series);
            }
            while (PlotModel.Annotations.Any(ser => (string)ser.Tag == plan.Id))
            {
                var annotation = FindAnnotation(plan.Id);
                PlotModel.Annotations.Remove(annotation);
            }
            UpdatePlot();
        }

        // Clear previous plot model
        public void OnClear()
        {
            PlotModel.Axes.Clear();
            PlotModel.Series.Clear();
            PlotModel.InvalidatePlot(true);
        }

        // Clear all plot series when input is changed
        public void RemoveAllSeries()
        {
            PlotModel.Series.Clear();
            PlotModel.InvalidatePlot(true);
        }

        // Export plot as pdf
        public void ExportPlotAsPdf(string filePath)
        {
            using (var stream = File.Create(filePath))
            {
                PdfExporter.Export(PlotModel, stream, 600, 400);
            }
        }

        // Export plotted data as txt file
        public void ExportDataAsTxt(string filePath)
        {
            using (var sw = new StreamWriter(filePath))
            {
                foreach (var ser in PlotModel.Series)
                {
                    if (OxyPlot.TypeExtensions.Equals(ser.GetType(), new OxyPlot.Series.ScatterSeries().GetType()))
                    {
                        sw.WriteLine(string.Format("#{0}", ser.Title));
                        var dataSer = (OxyPlot.Series.ScatterSeries)ser;
                        foreach (var point in dataSer.Points)
                        {
                            sw.WriteLine(string.Format("{0}\t{1}", point.X, point.Y));
                        }
                    }

                }
            }
        }
        #endregion

        // -------------- Private methods to create plot series ------------------------------
        #region UncertaintyScatterPlotSeries
        private void AddMeanDoseScatterSeries(PlanSetup plan, Structure structure)
        {
            // nominal dose
            DVHData dvh = plan.GetDVHCumulativeData(structure, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);
            double nomDose = dvh.MeanDose.Dose;

            var nomSeries = new OxyPlot.Series.ScatterSeries();
            nomSeries.Title = "Nominal";
            nomSeries.Tag = plan.Id;
            nomSeries.XAxisKey = "Linear";
            nomSeries.YAxisKey = "Dose [Gy]";
            var nominalPoint = new OxyPlot.Series.ScatterPoint(PlanIndex(plan), nomDose);
            nominalPoint.Tag = "Nominal";
            nomSeries.Points.Add(nominalPoint);
            nomSeries.MarkerSize = 10;
            nomSeries.MarkerFill = OxyColors.DarkRed;
            nomSeries.MarkerType = MarkerType.Circle;
            PlotModel.Series.Add(nomSeries);

            if (!plan.PlanUncertainties.Any(upln => upln.Dose != null) | !plan.PlanUncertainties.Any()) // if the plan does not have uncertainty scenarios calculated, return only the nominal dose
            {
                win.MessageBox.Show("Plan has no calculated uncertainty scenarios!");
                return;
            }

            // create list of dose values for all uncertainty plans
            foreach (var upln in plan.PlanUncertainties)
            {
                if (upln.Dose != null)
                {
                    DVHData udvh = upln.GetDVHCumulativeData(structure, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);

                    if (udvh == null)
                    {
                        win.MessageBox.Show("Uncertainty DVHs must be calculated in order to be accessible to this script!");
                        return;
                    }

                    var tempDose = udvh.MeanDose.Dose;
                    var tempSeries = new OxyPlot.Series.ScatterSeries();
                    tempSeries.Title = Regex.Match(upln.DisplayName.ToString(), @"\d+\s(.+)").Groups[1].Value.ToString();
                    tempSeries.Tag = plan.Id;
                    tempSeries.XAxisKey = "Linear";
                    tempSeries.YAxisKey = "Dose [Gy]";
                    var tempPoint = new OxyPlot.Series.ScatterPoint(PlanIndex(plan), tempDose);
                    tempPoint.Tag = Regex.Match(upln.DisplayName.ToString(), @"\d+\s(.+)").Groups[1].Value.ToString();
                    tempSeries.Points.Add(tempPoint);
                    tempSeries.MarkerSize = 10;
                    tempSeries.MarkerFill = OxyColors.Black;
                    tempSeries.MarkerType = MarkerType.Circle;
                    PlotModel.Series.Add(tempSeries);
                }
            }
        }

        private void AddDxDoseScatterSeries(PlanSetup plan, Structure structure, double volume, bool volIsAbs)
        {

            double nomDose = 0;

            // nominal dose
            DVHData dvh = plan.GetDVHCumulativeData(structure, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);

            if (volIsAbs)
                nomDose = DoseFromDVH(dvh, structure, volume);
            else
                nomDose = DoseFromDVH(dvh, structure, volume * structure.Volume / 100);

            var nomSeries = new OxyPlot.Series.ScatterSeries();
            nomSeries.Title = "Nominal";
            nomSeries.Tag = plan.Id;
            var nominalPoint = new OxyPlot.Series.ScatterPoint(PlanIndex(plan), nomDose);
            nominalPoint.Tag = "Nominal";
            nomSeries.XAxisKey = "Linear";
            nomSeries.YAxisKey = "Dose [Gy]";
            nomSeries.Points.Add(nominalPoint);
            nomSeries.MarkerSize = 10;
            nomSeries.MarkerFill = OxyColors.DarkRed;
            nomSeries.MarkerType = MarkerType.Circle;
            PlotModel.Series.Add(nomSeries);

            if (!plan.PlanUncertainties.Any(upln => upln.Dose != null)) // if the plan does not have uncertainty scenarios calculated, return only the nominal dose
            {
                win.MessageBox.Show("Plan has no calculated uncertainty scenarios!");
                return;
            }

            // create list of dose values for all uncertainty plans
            foreach (var upln in plan.PlanUncertainties)
            {
                if (upln.Dose != null)
                {
                    double tempDose = 0;
                    DVHData udvh = upln.GetDVHCumulativeData(structure, DoseValuePresentation.Absolute, VolumePresentation.AbsoluteCm3, 0.1);
                    if (udvh == null)
                    {
                        win.MessageBox.Show("Uncertainty DVHs must be calculated in order to be accessible to this script!");
                        return;
                    }

                    if (volIsAbs)
                        tempDose = DoseFromDVH(udvh, structure, volume);
                    else
                        tempDose = DoseFromDVH(udvh, structure, volume * structure.Volume / 100);

                    var tempSeries = new OxyPlot.Series.ScatterSeries();
                    tempSeries.Title = Regex.Match(upln.DisplayName.ToString(), @"\d+\s(.+)").Groups[1].Value.ToString();
                    tempSeries.Tag = plan.Id;
                    tempSeries.XAxisKey = "Linear";
                    tempSeries.YAxisKey = "Dose [Gy]";
                    var tempPoint = new OxyPlot.Series.ScatterPoint(PlanIndex(plan), tempDose);
                    tempPoint.Tag = Regex.Match(upln.DisplayName.ToString(), @"\d+\s(.+)").Groups[1].Value.ToString();
                    tempSeries.Points.Add(tempPoint);
                    tempSeries.MarkerSize = 10;
                    tempSeries.MarkerFill = OxyColors.Black;
                    tempSeries.MarkerType = MarkerType.Circle;
                    PlotModel.Series.Add(tempSeries);
                }
            }
        }

        private void AddVxScatterSeries(PlanSetup plan, Structure structure, double dose, bool isAbs)
        {

            double nomVol = 0;

            // nominal dose
            DVHData dvh = plan.GetDVHCumulativeData(structure, DoseValuePresentation.Absolute, VolumePresentation.Relative, 0.1);

            if (isAbs)
                nomVol = VolumeFromDVH(dvh, dose);
            else
                nomVol = VolumeFromDVH(dvh, dose * plan.TotalDose.Dose / 100);

            var nomSeries = new OxyPlot.Series.ScatterSeries();
            nomSeries.Title = "Nominal";
            nomSeries.Tag = plan.Id;
            nomSeries.XAxisKey = "Linear";
            nomSeries.YAxisKey = "Volume [%]";
            var nominalPoint = new OxyPlot.Series.ScatterPoint(PlanIndex(plan), nomVol);
            nominalPoint.Tag = "Nominal";
            nomSeries.Points.Add(nominalPoint);
            nomSeries.MarkerSize = 10;
            nomSeries.MarkerFill = OxyColors.DarkRed;
            nomSeries.MarkerType = MarkerType.Circle;
            PlotModel.Series.Add(nomSeries);

            if (!plan.PlanUncertainties.Any(upln => upln.Dose != null)) // if the plan does not have uncertainty scenarios calculated, return only the nominal dose
            {
                win.MessageBox.Show("Plan has no calculated uncertainty scenarios!");
                return;
            }

            // create list of dose values for all uncertainty plans
            foreach (var upln in plan.PlanUncertainties)
            {
                if (upln.Dose != null)
                {
                    double tempVol = 0;
                    DVHData udvh = upln.GetDVHCumulativeData(structure, DoseValuePresentation.Absolute, VolumePresentation.Relative, 0.1);
                    if (udvh == null)
                    {
                        win.MessageBox.Show("Uncertainty DVHs must be calculated in order to be accessible to this script!");
                        return;
                    }

                    if (isAbs)
                        tempVol = VolumeFromDVH(udvh, dose);
                    else
                        tempVol = VolumeFromDVH(udvh, dose * plan.TotalDose.Dose / 100);

                    var tempPoint = new OxyPlot.Series.ScatterPoint(PlanIndex(plan), tempVol);
                    var tempSeries = new OxyPlot.Series.ScatterSeries();
                    tempSeries.Title = Regex.Match(upln.DisplayName.ToString(), @"\d+\s(.+)").Groups[1].Value.ToString();
                    tempSeries.Tag = plan.Id;
                    tempSeries.XAxisKey = "Linear";
                    tempSeries.YAxisKey = "Volume [%]";
                    tempPoint.Tag = Regex.Match(upln.DisplayName.ToString(), @"\d+\s(.+)").Groups[1].Value.ToString();
                    tempSeries.Points.Add(tempPoint);
                    tempSeries.MarkerSize = 10;
                    tempSeries.MarkerFill = OxyColors.Black;
                    tempSeries.MarkerType = MarkerType.Circle;
                    PlotModel.Series.Add(tempSeries);
                }
            }
        }
        #endregion
        #region HelperMethods
        // helper methods
        private Structure GetStructure(PlanSetup plan, string id)
        {
            try
            {
                return plan.StructureSet.Structures.FirstOrDefault(s => s.Id == id);
            }
            catch
            {
                win.MessageBox.Show("Chosen structure could not be found in the plan's structure set. Showing first structure in plan's set");
                return plan.StructureSet.Structures.First();
            }
        }

        /// <summary>
        /// Interpolates dvh curve to return dose for a given volume
        /// </summary>
        /// <param name="dvh"></param>
        /// <param name="structure"></param>
        /// <param name="value">Volume in cc, if DVH is in absolute values</param>
        /// <returns></returns>
        private double DoseFromDVH(DVHData dvh, Structure structure, double value)
        {
            double ans = 0;
            List<DVHPoint> curve = dvh.CurveData.ToList();
            if (curve.Any(i => i.Volume <= value))
            {
                int index = curve.IndexOf(curve.First(i => i.Volume <= value));
                if (index > 0)
                {
                    ans = curve[index - 1].DoseValue.Dose + (value - curve[index - 1].Volume) *
                            (curve[index].DoseValue.Dose - curve[index - 1].DoseValue.Dose) /
                            (curve[index].Volume - curve[index - 1].Volume);
                }
                else
                {
                    ans = curve[index].DoseValue.Dose;
                }
            }
            return ans;
        }

        /// <summary>
        /// Interpolates dvh curve to return volume for a given dose
        /// If the requested dose is higher than the last item in the DVH curve then none of the contour
        /// receives this dose and 0 is returned.
        /// </summary>
        /// <param name="dvh"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        private double VolumeFromDVH(DVHData dvh, double value)
        {
            double ans;
            List<DVHPoint> curve = dvh.CurveData.ToList();
            if (value <= curve.Last().DoseValue.Dose)
            {
                int index = curve.IndexOf(curve.First(i => i.DoseValue.Dose >= value));
                ans = curve[index - 1].Volume + (value - curve[index - 1].DoseValue.Dose) *
                (curve[index].Volume - curve[index - 1].Volume) /
                (curve[index].DoseValue.Dose - curve[index - 1].DoseValue.Dose);
            }
            else
            {
                ans = 0;
            }
            return ans;
        }

        #endregion
    }
}

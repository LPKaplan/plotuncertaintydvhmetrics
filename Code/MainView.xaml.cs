﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using System.Reflection;
using Microsoft.Win32;
using OxyPlot.Wpf;
using OxyPlot;
using OxyPlot.Pdf;
using VMS.TPS.Common.Model.Types;
using VMS.TPS.Common.Model.API;

namespace PlotUncertaintyDVHMetrics
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class MainView : UserControl
    {
        // Fields
        private readonly MainViewModel _vm;
        private readonly string currentPath;

        // Constructor
        public MainView(MainViewModel viewModel)
        {
            _vm = viewModel;

            InitializeComponent();
            DataContext = viewModel;

            // Load variables from previous session
            LoadSavedVariables();
        }

        // Method to load previously saved variables
        private void LoadSavedVariables()
        {
            _vm.NumericInput = Properties.Settings.Default.numericInput;
        }

        #region Plot selection logic
        private void PlotsComboBoxLoaded(object sender, RoutedEventArgs e)
        {
            // List of possible plot to choose from. Expand as they are coded.
            List<string> plots = new List<string>();
            plots.Add("Empty");
            plots.Add("Mean structure dose for all uncertainty scenarios");
            plots.Add("Dx for all uncertainty scenarios (relative volume)");
            plots.Add("Dx for all uncertainty scenarios (absolute volume)");
            plots.Add("Vx for all uncertainty scenarios (relative dose)");
            plots.Add("Vx for all uncertainty scenarios (absolute dose)");


            // get the combo box reference
            var comboBox = sender as ComboBox;

            // assign the items source as the list
            comboBox.ItemsSource = plots;

            // Load last displayed plot. Default value: Empty
            var value = Properties.Settings.Default.chosenPlot;
            comboBox.SelectedIndex = plots.IndexOf(value);
        }

        private void PlotsSelectionChanged(object sender, RoutedEventArgs e)
        {
            var comboBox = (ComboBox)sender;

            // Uncheck all plan boxes
            foreach (var pn in _vm.PlanCollection)
            {
                pn.IsChecked = false;
            }

            // Clear plot
            _vm.OnClear();

            // Hide all optional elements
            HideAll();

            // Show optional elements according to plot type
            string value = comboBox.SelectedItem as string;
            switch (value)
            {
                case "Empty":
                    PlotDescription.Text = "No plot is shown. Choose a plot from the drop down menu.";
                    break;

                case "Mean structure dose for all uncertainty scenarios":
                    PlotDescription.Text = "Boxplot showing the mean dose to the chosen structure in all calculated plan uncertainty scenarios. Black line: nominal plan. If no uncertainty scenarios are calculated the nominal plan's mean dose is shown as a black dot.";
                    break;

                case "Dx for all uncertainty scenarios (relative volume)":
                    PlotDescription.Text = "Dose at a given volume for all calculated uncertainty scenarios. Volume is taken as percentage of a structure's volume.";
                    NumericInputBox.Visibility = Visibility.Visible;
                    InputButton.Visibility = Visibility.Visible;
                    EnterRelativeVolumeTitle.Visibility = Visibility.Visible;
                    break;

                case "Dx for all uncertainty scenarios (absolute volume)":
                    PlotDescription.Text = "Dose at a given volume for all calculated uncertainty scenarios. Volume is taken as absolute value in cc.";
                    NumericInputBox.Visibility = Visibility.Visible;
                    InputButton.Visibility = Visibility.Visible;
                    EnterAbsoluteVolumeTitle.Visibility = Visibility.Visible;
                    break;

                case "Vx for all uncertainty scenarios (relative dose)":
                    PlotDescription.Text = "Volume at a given dose for all calculated uncertainty scenarios. Dose is taken as percentage of the prescription dose.";
                    NumericInputBox.Visibility = Visibility.Visible;
                    InputButton.Visibility = Visibility.Visible;
                    EnterRelativeDoseTitle.Visibility = Visibility.Visible;
                    break;

                case "Vx for all uncertainty scenarios (absolute dose)":
                    PlotDescription.Text = "Volume at a given dose for all calculated uncertainty scenarios. Dose is taken as absolute value in Gy.";
                    NumericInputBox.Visibility = Visibility.Visible;
                    InputButton.Visibility = Visibility.Visible;
                    EnterAbsoluteDoseTitle.Visibility = Visibility.Visible;
                    break;
            }

            // Set plot type property in the view model
            _vm.ChosenPlotType = value;

            // Save selected value to file
            Properties.Settings.Default.chosenPlot = value;

            // Show plot
            _vm.ShowPlot();
        }
        #endregion

        #region Structure selection logic
        // Set comboBox selection at startup
        private void FindSelectedStructure(object comboBoxObject, RoutedEventArgs e)
        {
            var comboBox = (ComboBox)comboBoxObject;
            // load last selected structure. Default: BODY or first structure in the set
            var value = Properties.Settings.Default.chosenStructure;
            try
            {
                comboBox.SelectedItem = _vm.Structures.FirstOrDefault(s => s.Id == value);
            }
            catch
            {
                comboBox.SelectedIndex = 0;
            }
        }

        // Upon change of selection
        private void StructureSelected(object comboBoxObject, RoutedEventArgs e)
        {
            // Uncheck all plan boxes
            foreach (var pn in _vm.PlanCollection)
            {
                pn.IsChecked = false;
            }

            // Remove all series from plot
            _vm.RemoveAllSeries();

            var structure = GetStructure(comboBoxObject);
            string Id = structure.Id;
            _vm.ChosenStructureId = Id;

            // Save selected value to properties
            Properties.Settings.Default.chosenStructure = Id;
        }

        private Structure GetStructure(object comboBoxObject)
        {
            var comboBox = (ComboBox)comboBoxObject;
            var structure = (Structure)comboBox.SelectedItem;
            return structure;
        }
        #endregion

        #region Plan selection logic
        private void Plan_OnChecked(object checkBoxObject, RoutedEventArgs e)
        {
            _vm.AddPlanItem(GetPlan(checkBoxObject));
        }

        private void Plan_OnUnchecked(object checkBoxObject, RoutedEventArgs e)
        {
            _vm.RemovePlanItem(GetPlan(checkBoxObject));
        }

        private PlanSetup GetPlan(object checkBoxObject)
        {
            var checkbox = (CheckBox)checkBoxObject;
            var planName = (PlanName)checkbox.DataContext;
            var plan = _vm.PlansInScope.FirstOrDefault(p => p.Id == planName.Name);
            return plan;
        }
        #endregion

        #region Optional input logic
        // Hide all optional fields
        private void HideAll()
        {
            NumericInputBox.Visibility = Visibility.Collapsed;
            InputButton.Visibility = Visibility.Collapsed;

            EnterAbsoluteDoseTitle.Visibility = Visibility.Collapsed;
            EnterAbsoluteVolumeTitle.Visibility = Visibility.Collapsed;
            EnterRelativeDoseTitle.Visibility = Visibility.Collapsed;
            EnterRelativeVolumeTitle.Visibility = Visibility.Collapsed;
        }

        private void InputButtonClick(object sender, RoutedEventArgs e)
        {
            // Uncheck all plan boxes
            foreach (var pn in _vm.PlanCollection)
            {
                pn.IsChecked = false;
            }

            // Remove all plot series
            _vm.RemoveAllSeries();

            var entry = NumericInputBox.Text;
            if (IsDecimal(entry))
            {
                _vm.NumericInput = Convert.ToDouble(entry);
                MessageBox.Show(string.Format("Input value set to {0}", entry));

                // Save current value to properties
                Properties.Settings.Default.numericInput = Convert.ToDouble(entry);
            }
            else
                MessageBox.Show("Input must be a decimal value!");
        }


        private bool IsDecimal(string input)
        {
            Decimal dummy;
            return Decimal.TryParse(input, out dummy);
        }
        #endregion

        #region Export logic
        private void ExportPlotAsPdf(object sender, RoutedEventArgs e)
        {
            var filePath = GetPdfSavePath();
            if (filePath != null)
                _vm.ExportPlotAsPdf(filePath);
        }

        private void ExportDataAsTxt(object sender, RoutedEventArgs e)
        {
            var filePath = GetTxtSavePath();
            if (filePath != null)
                _vm.ExportDataAsTxt(filePath);
        }

        private string GetPdfSavePath()
        {
            var saveFileDialog = new SaveFileDialog
            {
                Title = "Export to PDF",
                Filter = "PDF Files (*.pdf)|*.pdf"
            };

            var dialogResult = saveFileDialog.ShowDialog();

            if (dialogResult == true)
                return saveFileDialog.FileName;
            else
                return null;
        }

        private string GetTxtSavePath()
        {
            var saveFileDialog = new SaveFileDialog
            {
                Title = "Export plot data to txt file",
                Filter = "txt files (*.txt)|*.txt"
            };

            var dialogResult = saveFileDialog.ShowDialog();

            if (dialogResult == true)
                return saveFileDialog.FileName;
            else
                return null;
        }
        #endregion
    }
}


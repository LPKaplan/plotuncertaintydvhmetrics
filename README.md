# PlotUncertaintyDVHMetrics
This script uses a WPF UI and the C# package OxyPlot to plot DVH metrics for all calculated uncertainty scenarios for a plan.
See the Eclipse Scripting Application Programming Interface Online help for details on API methods and objects.
The PDF "Varian-API-handbook_July2018.pdf" gives an introduction to plotting with OxyPlot in chapter 5.

See User Instructions for further information.

